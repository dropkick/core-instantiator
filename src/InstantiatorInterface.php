<?php

namespace Dropkick\Core\Instantiator;

use Dropkick\Core\Invokable\ReflectorInterface;
use Dropkick\Core\Invokable\ResolverInterface;

/**
 * Interface InstantiatorInterface.
 *
 * Provides a method of instantiating a class.
 */
interface InstantiatorInterface {

  /**
   * Return the reflector.
   *
   * @return \Dropkick\Core\Invokable\ReflectorInterface
   *   The reflector object.
   */
  public function getReflector();

  /**
   * Set the reflector.
   *
   * @param \Dropkick\Core\Invokable\ReflectorInterface $reflector
   *   The reflector object.
   *
   * @return static
   *   The instantiator object.
   */
  public function setReflector(ReflectorInterface $reflector);

  /**
   * Return the resolver.
   *
   * @return \Dropkick\Core\Invokable\ResolverInterface
   *   The resolver object.
   */
  public function getResolver();

  /**
   * Set the resolver.
   *
   * @param \Dropkick\Core\Invokable\ResolverInterface $resolver
   *   The resolver object.
   *
   * @return static
   *   The instantiator object.
   */
  public function setResolver(ResolverInterface $resolver);

  /**
   * Instantiate a class.
   *
   * @param string $class
   *   The class name.
   * @param string|null $requirement
   *   The class or interface the object must match.
   *
   * @return object|string|null
   *   Returns an object for a class, string when class is static, or NULL when
   *   the class is unsupported.
   *
   * @throws \Dropkick\Core\Instantiator\InstantiationException
   */
  public function instantiate($class, $requirement = NULL);

}
