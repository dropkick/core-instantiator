<?php

namespace Dropkick\Core\Instantiator;

use Dropkick\Core\Formattable\FormattableString;

/**
 * Class Instantiator.
 *
 * A generic implementation of InstantiatorInterface that allows for other
 * instantiators to handle creating the object.
 */
class Instantiator implements InstantiatorInterface {
  use InstantiatorTrait;

  /**
   * The instantiators to attempt before going to default instantiation.
   *
   * @var \Dropkick\Core\Instantiator\InstantiatorInterface[]
   */
  protected $instantiators = [];

  /**
   * Add an instantiator.
   *
   * @param \Dropkick\Core\Instantiator\InstantiatorInterface $instantiator
   *   An instantiator object.
   *
   * @return static
   *   The instantiator object.
   */
  public function addInstantiator(InstantiatorInterface $instantiator) {
    $this->instantiators[] = $instantiator->setResolver($this->getResolver())
      ->setReflector($this->getReflector());
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function instantiate($class, $requirement = NULL) {
    foreach ($this->instantiators as $instantiator) {
      $result = $instantiator->instantiate($class, $requirement);
      if (!is_null($result)) {
        return $this->requirement($result, $requirement);
      }
    }
    return $this->default($class, $requirement);
  }

  /**
   * Return a created object.
   *
   * @param string $class
   *   The class.
   * @param string|null $requirement
   *   The class requirement.
   *
   * @return mixed
   *   The created object.
   *
   * @throws \Dropkick\Core\Instantiator\InstantiationException
   */
  protected function default($class, $requirement) {
    if (!class_exists($class)) {
      throw new InstantiationException(
        FormattableString::create(
          'Class "{{ class }}" does not exist.',
          ['class' => $class]
        )
      );
    }

    // Get the arguments for the instantiated class.
    $arguments = $this->getConstructorArgs($class);

    // Create the object.
    $object = $this->create($class, $arguments);

    // Process any requirements.
    return $this->requirement($object, $requirement);
  }

  /**
   * Performs a requirement check.
   *
   * @param mixed $object
   *   The created object.
   * @param string|null $requirement
   *   The requirement for the object.
   *
   * @return mixed
   *   The created object.
   *
   * @throws \Dropkick\Core\Instantiator\InstantiationException
   */
  protected function requirement($object, $requirement) {
    if (is_null($requirement)) {
      return $object;
    }

    if (!is_a($object, $requirement, TRUE) && !is_subclass_of($object, $requirement)) {
      throw new InstantiationException(
        FormattableString::create(
          '"{{ class }}" does not meet requirement "{{ requirement }}".',
          [
            'class' => is_string($object) ? $object : get_class($object),
            'requirement' => $requirement,
          ]
        )
      );
    }

    return $object;
  }

  /**
   * Create the class based on the appropriate behaviour.
   *
   * @param string $class
   *   The class to be initialized.
   * @param array $args
   *   The arguments for the class.
   *
   * @return object
   *   The instantiated object.
   */
  protected function create($class, array $args) {
    // Create according the number of arguments, to speed up creation.
    switch (count($args)) {
      case 0:
        return new $class();

      case 1:
        return new $class($args[0]);

      case 2:
        return new $class($args[0], $args[1]);

      case 3:
        return new $class($args[0], $args[1], $args[2]);

      case 4:
        return new $class($args[0], $args[1], $args[2], $args[3]);

      case 5:
        return new $class($args[0], $args[1], $args[2], $args[3], $args[4]);

      case 6:
        return new $class($args[0], $args[1], $args[2], $args[3], $args[4],
          $args[5]);

      case 7:
        return new $class($args[0], $args[1], $args[2], $args[3], $args[4],
          $args[5], $args[6]);

      case 8:
        return new $class($args[0], $args[1], $args[2], $args[3], $args[4],
          $args[5], $args[6], $args[7]);

      case 9:
        return new $class($args[0], $args[1], $args[2], $args[3], $args[4],
          $args[5], $args[6], $args[7], $args[8]);

      case 10:
        return new $class($args[0], $args[1], $args[2], $args[3], $args[4],
          $args[5], $args[6], $args[7], $args[8], $args[9]);

      case 11:
        return new $class($args[0], $args[1], $args[2], $args[3], $args[4],
          $args[5], $args[6], $args[7], $args[8], $args[9], $args[10]);

      case 12:
        return new $class($args[0], $args[1], $args[2], $args[3], $args[4],
          $args[5], $args[6], $args[7], $args[8], $args[9], $args[10],
          $args[11]);

      case 13:
        return new $class($args[0], $args[1], $args[2], $args[3], $args[4],
          $args[5], $args[6], $args[7], $args[8], $args[9], $args[10],
          $args[11], $args[12]);

      case 14:
        return new $class($args[0], $args[1], $args[2], $args[3], $args[4],
          $args[5], $args[6], $args[7], $args[8], $args[9], $args[10],
          $args[11], $args[12], $args[13]);

      case 15:
        return new $class($args[0], $args[1], $args[2], $args[3], $args[4],
          $args[5], $args[6], $args[7], $args[8], $args[9], $args[10],
          $args[11], $args[12], $args[13], $args[14]);
    }

    // There are too many arguments revert back to using Reflection class
    // creation.
    $reflection = new \ReflectionClass($class);
    return $reflection->newInstanceArgs($args);
  }

}
