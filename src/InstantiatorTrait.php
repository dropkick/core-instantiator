<?php

namespace Dropkick\Core\Instantiator;

use Dropkick\Core\Formattable\FormattableString;
use Dropkick\Core\Invokable\Reflector;
use Dropkick\Core\Invokable\ReflectorInterface;
use Dropkick\Core\Invokable\Resolver;
use Dropkick\Core\Invokable\ResolverInterface;

/**
 * Trait InstantiatorTrait.
 *
 * Provides the standard constructor arguments used for instantiating a class.
 */
trait InstantiatorTrait {

  /**
   * The resolver object.
   *
   * @var \Dropkick\Core\Invokable\ResolverInterface
   */
  protected $resolver;

  /**
   * The reflector object.
   *
   * @var \Dropkick\Core\Invokable\ReflectorInterface
   */
  protected $reflector;

  /**
   * Get the reflector object.
   *
   * @see \Dropkick\Core\Instantiator\InstantiatorInterface::getReflector()
   */
  public function getReflector() {
    if (!isset($this->reflector)) {
      $this->reflector = new Reflector();
    }
    return $this->reflector;
  }

  /**
   * Set the reflector object.
   *
   * @see \Dropkick\Core\Instantiator\InstantiatorInterface::setReflector()
   */
  public function setReflector(ReflectorInterface $reflector) {
    $this->reflector = $reflector;
    return $this;
  }

  /**
   * Get the resolver object.
   *
   * @see \Dropkick\Core\Instantiator\InstantiatorInterface::getResolver()
   */
  public function getResolver() {
    if (!isset($this->resolver)) {
      $this->resolver = new Resolver();
    }
    return $this->resolver;
  }

  /**
   * Set the resolver object.
   *
   * @see \Dropkick\Core\Instantiator\InstantiatorInterface::setResolver()
   */
  public function setResolver(ResolverInterface $resolver) {
    $this->resolver = $resolver;
    return $this;
  }

  /**
   * Return the constructor arguments.
   *
   * @param string $class
   *   The class constructor.
   *
   * @return array
   *   The argument values.
   *
   * @throws \Dropkick\Core\Instantiator\InstantiationException
   */
  protected function getConstructorArgs($class) {
    $arguments = [];

    // Ensure there is a constructor.
    if (!(new \ReflectionClass($class))->getConstructor()) {
      return $arguments;
    }

    // Map all the constructor arguments using the resolver.
    $resolver = $this->getResolver();
    foreach ($this->getReflector()->getArguments([$class, '__construct']) as $arg) {
      if ($resolver->applies($arg)) {
        $arguments[] = $resolver->getValue($arg);
      }
      elseif (!$arg->hasDefault()) {
        throw new InstantiationException(
          FormattableString::create(
            'Undefined argument value'
          )
        );
      }
    }
    return $arguments;
  }

}
