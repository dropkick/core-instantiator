<?php

namespace Dropkick\Core\Instantiator;

/**
 * Class InstantiationException.
 *
 * Triggered when a created object does not match the expected interface or
 * class requirements.
 */
class InstantiationException extends \Exception {
}
