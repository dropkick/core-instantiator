<?php

namespace Dropkick\Core\Instantiator;

use Dropkick\Core\Invokable\ArgumentInterface;
use Dropkick\Core\Invokable\Reflector;
use Dropkick\Core\Invokable\ResolverInterface;
use PHPUnit\Framework\Test;
use PHPUnit\Framework\TestCase;

class InstantiatorTest extends TestCase {

  /**
   * @var \Dropkick\Core\Instantiator\Instantiator
   */
  protected $instantiator;

  public function setUp(): void {
    $this->instantiator = new Instantiator();
  }

  public function testClassDoesNotExist() {
    $class = self::class . 'Undefined';
    $this->expectException(InstantiationException::class);
    $this->instantiator->instantiate($class);
  }

  public function testClassInterfaceInvalid() {
    $class = self::class;
    $this->expectException(InstantiationException::class);
    $this->instantiator->instantiate($class, InstantiatorInterface::class);
  }

  public function testClassRequirementInvalid() {
    $class = self::class;
    $this->expectException(InstantiationException::class);
    $this->instantiator->instantiate($class, Instantiator::class);
  }
  
  public function testClass() {
    $class = self::class;
    $object = $this->instantiator->instantiate($class);
    $this->assertEquals($class, get_class($object));
  }

  public function testClassInterface() {
    $class = self::class;
    $object = $this->instantiator->instantiate($class, Test::class);
    $this->assertEquals($class, get_class($object));
  }

  public function testClassRequirement() {
    $class = self::class;
    $object = $this->instantiator->instantiate($class, TestCase::class);
    $this->assertEquals($class, get_class($object));
  }

  public function testDepClass() {
    $class = DummyInstantiator::class;
    $this->instantiator->addInstantiator(new DummyInstantiator());
    $object = $this->instantiator->instantiate($class);
    $this->assertEquals($class, get_class($object));
  }

  public function testDepClassInterface() {
    $class = DummyInstantiator::class;
    $this->instantiator->addInstantiator(new DummyInstantiator());
    $object = $this->instantiator->instantiate($class, InstantiatorInterface::class);
    $this->assertEquals($class, get_class($object));
  }

  public function testDepClassRequirement() {
    $class = DummyInstantiator::class;
    $this->instantiator->addInstantiator(new DummyInstantiator());
    $object = $this->instantiator->instantiate($class, DummyInstantiator::class);
    $this->assertEquals($class, get_class($object));
  }

  public function providerArgClasses() {
    $values = [];
    for ($i = 0; $i < 20; $i++) {
      $values[] = [
        'class' => TestArguments::class,
        'arguments' => $i ? range(1, $i) : [],
      ];
    }
    return $values;
  }

  /**
   * @dataProvider providerArgClasses
   */
  public function testMultipleArguments($class, $arguments) {
    $this->instantiator->setReflector(new Reflector());
    $this->instantiator->setResolver(new TestResolver($arguments));
    $object = $this->instantiator->instantiate($class);
    $this->assertEquals(TestArguments::class, get_class($object));
    $this->assertEquals($arguments, $object->arguments);
  }

  public function testNoArgumentValue() {
    $this->expectException(InstantiationException::class);
    $this->instantiator->instantiate(TestNoArgument::class);
  }

  public function testNoConstructor() {
    $class = DummyInstantiator::class;
    $object = $this->instantiator->instantiate($class);
    $this->assertEquals(DummyInstantiator::class, get_class($object));
  }
}

class DummyInstantiator implements InstantiatorInterface {
  use InstantiatorTrait;

  public function instantiate($class, $requirement = NULL) {
    if ($class === self::class) {
      return new $class;
    }
    return NULL;
  }
}

class TestArguments {
  public $arguments;

  public function __construct(
    $a0 = NULL,
    $a1 = NULL,
    $a2 = NULL,
    $a3 = NULL,
    $a4 = NULL,
    $a5 = NULL,
    $a6 = NULL,
    $a7 = NULL,
    $a8 = NULL,
    $a9 = NULL,
    $a10 = NULL,
    $a11 = NULL,
    $a12 = NULL,
    $a13 = NULL,
    $a14 = NULL,
    $a15 = NULL,
    $a16 = NULL,
    $a17 = NULL,
    $a18 = NULL,
    $a19 = NULL,
    $a20 = NULL,
    $a21 = NULL
  ) {
    $this->arguments = func_get_args();
  }
}

class TestNoArgument {
  public function __construct($arg) {
  }
}

class TestResolver implements ResolverInterface {
  protected $args;

  public function __construct(array $args) {
    $this->args = $args;
  }

  public function applies(ArgumentInterface $argument) {
    return array_key_exists($argument->getPosition(), $this->args);
  }

  public function getValue(ArgumentInterface $argument) {
    return $this->args[$argument->getPosition()];
  }

}
